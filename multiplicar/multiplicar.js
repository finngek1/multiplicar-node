const fs = require('fs');

let listarTebla = (base, limite = 10) => {
    let data = '';
    return new Promise ((resolve, reject) => {

        if(!Number(base) || !Number(limite)) {
            reject('la base o el limite NO es un numero');
            return;
        }

        for(let i = 1; i <= limite; i++) {
            data += `${base} * ${i} = ${base*i} \n`
        }

        resolve(data);
        return;
    });
}

let crearArchivo = async(base, limite = 10) => {
    let data = '';

    console.log('Tabla de multiplacar base ' + base);

    return new Promise ((resolve, reject) => {

        if(!Number(base) || !Number(limite)) {
            reject('la base o el limite NO es un numero');
            return;
        }

        for(let i = 1; i <= limite; i++) {
            data += `${base} * ${i} = ${base*i} \n`
        }
    
        fs.writeFile(`tablas/tabla-${base}.txt`, data, (err) => {
            if (err) {
                reject(err); 
                return;
            } else {
                resolve(`tabla-${base}.txt`);
                return;
            }
        });
    });
}

module.exports = {
    crearArchivo,
    listarTebla
}